export const books: Books[] = [
    {
        id: 1,
        titre: "Harry Potter",
        pages: 400,
        auteur: "JK Rollinge"
    },
    {
        id: 2,
        titre: "Harry Potter 2",
        pages: 399,
        auteur: "JK Rollinge"
    },
    {
        id: 3,
        titre: "Harry Potter 12",
        pages: 398,
        auteur: "JK Rollinge"
    },
    {
        id: 4,
        titre: "Harry",
        pages: 200,
        auteur: "JK Rollinge Junior"
    },
]

export type Books = {
    id: number
    titre: string
    pages: number
    auteur: string
}
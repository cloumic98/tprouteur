import {v4} from "uuid";

export class Observer {

    private readonly _id: string
    private readonly _callback: Function

    constructor(callback: Function = () => {}) {
        this._id = v4()
        this._callback = callback
    }

    update(data: any): void {
        this._callback(data)
    }

    get id(): string {
        return this._id
    }
}

export class Subject {

    private _observer: Map<string, Observer>

    constructor() {
        this._observer = new Map<string, Observer>()
    }

    notify(data: any): void {
        this._observer.forEach(value => {
            value.update(data)
        })
    }

    subscribe(observer: Observer): void {
        this._observer.set(observer.id, observer)
    }

    unsubscribe(observer: Observer): void {
        this._observer.delete(observer.id)
    }
}
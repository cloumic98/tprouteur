import {Observer, Subject} from "../../Observer/Observer";
import {DetailsModel} from "./detailsModel";

export class DetailsView {
  private _detailsViewSubject: Subject
  private _detailsModelObserver: Observer
  private _mainDiv: HTMLDivElement;
  private _button: HTMLButtonElement;

  constructor() {
    this._mainDiv = document.createElement('div')
    this._button = document.createElement('button')

    this._mainDiv.appendChild(this._button)

    this._button.addEventListener('click', () => {
      this._detailsViewSubject.notify('click')
    })

    this._detailsViewSubject = new Subject()
    this._detailsModelObserver = new Observer((detailsModel: DetailsModel) => {
      this.renderView(detailsModel)
    })
  }
  get detailsViewSubject(): Subject { return this._detailsViewSubject}
  get detailsModelObserver(): Observer { return this._detailsModelObserver}
  get main(): HTMLHeadingElement { return this._mainDiv }

  renderView(detailsModel: DetailsModel){
      if(detailsModel.book != undefined)
      {
        this._mainDiv.innerHTML = `
          <h3>${detailsModel.book.id}</h3>
          <h3>${detailsModel.book.titre}</h3>
          <h3>${detailsModel.book.pages}</h3>
          <h3>${detailsModel.book.auteur}</h3>
        `
      }else{
        console.log('RIP')
      }
  }
}
import {Observer} from "../../observer/Observer";
import {DetailsModel} from "./detailsModel";
import {DetailsView} from "./detailsView";
import {Router} from "../../router/Router";

export class DetailsController {

  private _detailsModel: DetailsModel
  private _detailsView: DetailsView

  private _detailsViewObserver: Observer;

  constructor() {
    this._detailsModel = new DetailsModel()
    this._detailsView = new DetailsView()

    this._detailsModel.detailsModelSubject.subscribe(this._detailsView.detailsModelObserver)

    this._detailsViewObserver = new Observer(() => {
      this.update()
    })
    this._detailsView.detailsViewSubject.subscribe(this._detailsViewObserver)


  }
  get view(): DetailsView { return this._detailsView}

  init(){
    this._detailsView.renderView(this._detailsModel)
  }

  update(){
    Router.instance.changeRoute('liste')
  }
}
import {Subject} from "../../observer/Observer";
import {books, Books} from "../../mock/Books";
import {Router} from "../../router/Router";

export class DetailsModel {

  private _book: Books[]
  private _detailsModelSubject: Subject

  constructor() {
    this._book = books
    this._detailsModelSubject = new Subject()
  }

  get detailsModelSubject(): Subject { return this._detailsModelSubject}

  get book(): Books {
    return this._book.find((book: Books) => {
      return book.id == parseInt(Router.instance.paramsMap.get("id"))
    })!
  }
}
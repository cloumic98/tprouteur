import {ListeModel} from "./listeModel";
import {ListeView} from "./listeView";
import {Observer} from "../../observer/Observer";

export class ListeController {
  private _listeModel: ListeModel
  private _listeView: ListeView

  private _listeViewObserver: Observer;

  constructor() {
    this._listeModel = new ListeModel()
    this._listeView = new ListeView()

    this._listeModel.listeModelSubject.subscribe(this._listeView.indexModelObserver)

    this._listeViewObserver = new Observer(() => {
      this.update()
    })

    this._listeView.indexViewSubject.subscribe(this._listeViewObserver)

    this._listeView.renderView(this._listeModel)
  }
  get view(): ListeView { return this._listeView}

  init(){}

  update(){
    // Router.instance.changeRoute('details/0372d604-43a8-4d9c-a146-f5444db51518')
  }
}
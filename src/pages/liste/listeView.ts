import {ListeModel} from "./listeModel";
import {Observer, Subject} from "../../observer/Observer";
import {Books} from "../../mock/Books";

export class ListeView {

  private _listeViewSubject: Subject
  private _listeModelObserver: Observer

  // DISPLAY ELEMENTS
  private _mainDiv: HTMLDivElement;

  constructor() {
    this._mainDiv = document.createElement('div')

    this._listeViewSubject = new Subject()
    this._listeModelObserver = new Observer((listeModel: ListeModel) => {
      this.renderView(listeModel)
    })
  }
  get indexViewSubject(): Subject { return this._listeViewSubject}
  get indexModelObserver(): Observer { return this._listeModelObserver}
  get main(): HTMLHeadingElement { return this._mainDiv }

  renderView(listeModel: ListeModel){
    listeModel.books.forEach((book: Books) => {
      this._mainDiv.innerHTML += `
        <h3>ID du livre : ${book.id}</h3>
        <ul>
            <li>Titre : ${book.titre}</li>
            <li>Nombre de pages : ${book.pages}</li>
            <li>Auteur : ${book.auteur}</li>
        </ul>
      `
    })
  }
}
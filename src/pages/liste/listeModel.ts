import {Subject} from "../../observer/Observer";
import {books, Books} from "../../mock/Books";

export class ListeModel {

  private _bookList: Books[]
  private _listeModelSubject: Subject

  constructor() {
    this._bookList = books
    this._listeModelSubject = new Subject()
  }

  get listeModelSubject(): Subject { return this._listeModelSubject}

  get books(): Books[] { return this._bookList}
}
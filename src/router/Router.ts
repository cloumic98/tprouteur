export interface Route {
    path: string, // Chemin qui se trouve après le #
    component: any // Controller des pages
}

export class Router {

    private static _instance: Router

    // D'avoir une map de route => controller
    private _routeList: Map<string, any>
    private _rootElement: HTMLDivElement;

    private _paramsList: Map<string, any>

    constructor() {
        // On vas chercher l'element dans l'html
        this._rootElement = document.querySelector<HTMLDivElement>('#app')!
        this._routeList = new Map<string, any>()
        this._paramsList = new Map<string, any>()
    }

    addRoute(route: Route){
        this._routeList.set(route.path, route.component)
    }

    loadRoute(){
        const findedUrl = this.testUrl()
        if(findedUrl != "-1"){
            const currentController = this._routeList.get(findedUrl)
            currentController.init()
            this._rootElement.appendChild(currentController.view.main)
        }else{
            console.warn("LA ROUTE EXISTE PO")
        }
    }

    testUrl(): string{
        // On découpe l'url de la page
        const locationPath = window.location.hash.slice(2)
        const locationParts = locationPath.split('/')

        // this._routeList.keys() -> Iterable -> pas pratique
        // Array.from(this._routeList.keys()) -> [] -> pratique
        const listPaths =  Array.from(this._routeList.keys())

        // pour tout les chemins vers des composants
        for(let p = 0; p < listPaths.length; p ++){
            // on découpe le chemin
            const urlParts = listPaths[p].split('/')
            // si la taille du chemin est identique à la taille du chemin de la page
            if(urlParts.length == locationParts.length){
                // on met une valeur à true
                let rightUrl = true

                const paramsMap = new Map<string, any>()

                // pour chaque partie du chemin
                urlParts.forEach((part, index) => {
                    // part => c'est la partie de l'url définie en lien avec le composant
                    // si part commence par un : on s'en fout
                    if(part[0] == ':'){
                        paramsMap.set(part.slice(1), locationParts[index])
                        // on fait rien
                    }// si non si elle pas égale à la partie du chemin
                    else if(part != locationParts[index]){
                        rightUrl = false // on false l'url
                    }
                })
                // si l'url trouver reste bonne
                if(rightUrl){
                    // on renvoie la valeur
                    this._paramsList = paramsMap
                    return listPaths[p]
                }
            }
        }
        return '-1'
    }

    get paramsMap(): Map<string, any> {
        return this._paramsList
    }

    static get instance(): Router {
        if(Router._instance == null){
            Router._instance = new Router()
        }
        return Router._instance
    }

    changeRoute(path: string) {
        window.location.hash = `#/${path}`
        while(this._rootElement.firstChild){
            this._rootElement.removeChild(this._rootElement.firstChild)
        }
        this.loadRoute()
    }
}

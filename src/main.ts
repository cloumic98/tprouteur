import './style.css'
import {Router} from "./router/Router";
import {ListeController} from "./pages/liste/listeController";
import {DetailsController} from "./pages/details/detailsController";

Router.instance.addRoute({
    path: 'liste',
    component: new ListeController()
})

Router.instance.addRoute({
    path: 'details/:id',
    component: new DetailsController()
})

Router.instance.loadRoute()